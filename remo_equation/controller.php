<?php      

defined('C5_EXECUTE') or die(_("Access Denied."));

class RemoEquationPackage extends Package {

	protected $pkgHandle = 'remo_equation';
	protected $appVersionRequired = '5.3.3';
	protected $pkgVersion = '1.0';
	
	public function getPackageDescription() {
		return t("Installs the equation block.");
	}
	
	public function getPackageName() {
		return t("Equation");
	}
	
	public function install() {
		$pkg = parent::install();
		
		// install block		
		BlockType::installBlockTypeFromPackage('remo_equation', $pkg);
		
	}




}