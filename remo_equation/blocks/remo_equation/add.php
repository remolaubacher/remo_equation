<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));
?>
<h2><?php   echo t('Equation')?></h2>

<textarea id="remo-equation-textarea" name="equation" style="width:430px;height: 100px;"></textarea>

<h2><?php   echo t('Preview')?></h2>
<a href="javascript:void()">Click here to refresh preview</a>

<div id="remo-equation-preview" style="margin-top: 5px;width:420px;height: 90px;border:1px solid gray;padding: 5px;">
</div>

<p>
For more information on the LaTeX math markup used in this block, please visit <a target="_blank" href="http://www.forkosh.com/mimetextutorial.html">http://www.forkosh.com/mimetextutorial.html</a>
</p>