<?php     
	defined('C5_EXECUTE') or die(_("Access Denied."));
	class RemoEquationBlockController extends BlockController {
				
		protected $btTable = 'btRemoEquation';
		protected $btInterfaceWidth = "450";
		protected $btInterfaceHeight = "365";
		
		public function getBlockTypeDescription() {
			return t("Inserts an equation using mathTeX.");
		}
		
		public function getBlockTypeName() {
			return t("Equation");
		}	
		
	}
	
?>